<?php

namespace Drupal\oauth2_token_manager\Commands;

use Drupal\oauth2_token_manager\TokenManager;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 */
class TokenGeneratorCommandsBase extends DrushCommands {

  /**
   * Token manager service.
   *
   * @var \Drupal\oauth2_token_manager\TokenManager
   */
  protected $tokenManager;

  /**
   * Creates a new TokenGeneratorCommands instance.
   *
   * @param \Drupal\oauth2_token_manager\TokenManager $token_manager
   *   Token manager service.
   */
  public function __construct(TokenManager $token_manager) {
    $this->tokenManager = $token_manager;
  }

  /**
   * Sets a new refresh token.
   *
   * @param string $refresh_token
   *   The new refresh token.
   *
   * @throws \Exception
   */
  public function setRefreshToken($refresh_token): void {
    if (!$this->tokenManager->setSystemRefreshToken($refresh_token)) {
      throw new \Exception('Using refresh token failed.');
    }

    $this->logger()->success(dt('Refresh token successfully updated.'));
  }

  /**
   * Initializes a new refresh token.
   */
  public function initRefreshToken(): void {
    $authUrl = $this->tokenManager->initSystemAccessToken();
    $this->logger()->success(dt('Please visit the following URL to initialize the system token: {url}', ['url' => $authUrl]));
  }

}
