<?php

namespace Drupal\oauth2_token_manager;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Base class to register arguments for services in this module.
 */
class Oauth2TokenManagerServiceProviderBase extends ServiceProviderBase {

  /**
   * Perform the environment variable replacement altering.
   *
   * @param \Drupal\Core\DependencyInjection\ContainerBuilder $container
   *   The container.
   * @param string $parameter_key
   *   The parameter key.
   * @param string $env_prefix
   *   The env variable prefix.
   */
  public function doAlter(ContainerBuilder $container, string $parameter_key, string $env_prefix): void {
    $options = $container->getParameterBag()->get($parameter_key);
    foreach (getenv() as $key => $value) {
      if (strpos($key, $env_prefix) === 0) {
        // Convert the key to the right configuration value.
        $options_key = lcfirst(ContainerBuilder::camelize(strtolower(substr($key, mb_strlen($env_prefix)))));
        if (isset($options[$options_key])) {
          $options[$options_key] = $value;
        }
      }
    }
    $container->setParameter($parameter_key, $options);
  }

}
