<?php

namespace Drupal\oauth2_token_manager\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\oauth2_token_manager\TokenManager;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ValidationControllerBase.
 */
class ValidationControllerBase extends ControllerBase {

  /**
   * The token manager.
   *
   * @var \Drupal\oauth2_token_manager\TokenManager
   */
  protected $tokenManager;

  /**
   * ValidationControllerBase constructor.
   *
   * @param \Drupal\oauth2_token_manager\TokenManager $token_manager
   *   The token manager.
   */
  public function __construct(TokenManager $token_manager) {
    $this->tokenManager = $token_manager;
  }

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function validate(Request $request): array {
    // User token.
    if (!empty($_SESSION['oauth2state'])) {
      // @todo Forward to original destination from $_SESSION.
      return [
        '#cache' => ['max-age' => 0],
        '#type' => 'markup',
        '#markup' => $this->t('User OAuth2 placeholder'),
      ];
    }

    // System token.
    $oauth2_state = $request->get('state');
    $oauth2_code = $request->get('code');
    $state_parameter = $this->tokenManager->getInitState();

    if ($state_parameter === $oauth2_state) {
      if ($this->tokenManager->useSystemAuthorizationCode($oauth2_code)) {
        $this->messenger()->addStatus('System refresh token successfully setup.');
      }
      else {
        $this->messenger()->addError('System refresh token setup failed. Code might have expired.');
      }
    }

    return [
      '#cache' => ['max-age' => 0],
      '#type' => 'markup',
      '#markup' => $this->t('OAuth2 - Validation'),
    ];
  }

}
