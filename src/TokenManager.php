<?php

namespace Drupal\oauth2_token_manager;

use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\State\StateInterface;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Token\AccessToken;
use Psr\Log\LoggerInterface;

/**
 * Provide token management functionality.
 */
class TokenManager {

  /**
   * OAuth2 service.
   *
   * @var \League\OAuth2\Client\Provider\AbstractProvider
   */
  protected $oauth2Provider;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The initialization name for the system token.
   *
   * @var string
   */
  protected $systemTokenInitName;

  /**
   * The name of the system token.
   *
   * @var string
   */
  protected $systemTokenName;

  /**
   * The lock service.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * The lock name.
   *
   * @var string
   */
  protected $lockName;

  /**
   * The scopes to request during token generation.
   *
   * @var string[]
   */
  protected $scopes;

  /**
   * Creates a new TokenManager instance.
   *
   * @param \League\OAuth2\Client\Provider\AbstractProvider $oauth2_provider
   *   The OAuth2 provider instance.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\State\StateInterface $state
   *   State service.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock service.
   * @param string $token_prefix
   *   The token prefix for storing in cache; should be unique.
   * @param array $scopes
   *   The OAuth2 scopes to grant.
   */
  public function __construct(AbstractProvider $oauth2_provider, LoggerInterface $logger, StateInterface $state, LockBackendInterface $lock, string $token_prefix, array $scopes = []) {
    $this->oauth2Provider = $oauth2_provider;
    $this->logger = $logger;
    $this->state = $state;
    $this->systemTokenInitName = "$token_prefix.system_token_init";
    $this->systemTokenName = "$token_prefix.system_token";
    $this->lock = $lock;
    $this->lockName = "$token_prefix..system_token:refresh";
    $this->scopes = $scopes;
  }

  /**
   * Sets a new system refresh token.
   *
   * @param string $refresh_token
   *   The new refresh token.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public function setSystemRefreshToken($refresh_token): bool {
    return $this->setSystemOauth2Token('refresh_token', ['refresh_token' => $refresh_token]);
  }

  /**
   * Uses a system refresh code to init the access_token.
   *
   * @param string $code
   *   The code that provided to the validate callback.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public function useSystemAuthorizationCode($code): bool {
    return $this->setSystemOauth2Token('authorization_code', ['code' => $code]);
  }

  /**
   * Helper function to call getAccessToken().
   *
   * @param string $name
   *   The name of the access token to retrieve.
   * @param array $options
   *   The options to pass to getAccessToken(), e.g. 'refresh_token' or 'code'.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  protected function setSystemOauth2Token($name, array $options): bool {
    $status = TRUE;

    try {
      $access_token = $this->oauth2Provider->getAccessToken($name, $options);
      $this->state->set($this->systemTokenName, $access_token);
    }
    catch (\Exception $e) {
      $status = FALSE;
    }

    return $status;
  }

  /**
   * Initializes the system access token.
   *
   * @return string
   *   The authentication URL the user needs to visit.
   */
  public function initSystemAccessToken(): string {
    $authUrl = $this->oauth2Provider->getAuthorizationUrl(['scope' => $this->scopes]);
    $state_parameter = $this->oauth2Provider->getState();
    $this->state->set($this->systemTokenInitName, $state_parameter);

    return $authUrl;
  }

  /**
   * Get the init state.
   *
   * @return string
   *   The init state.
   */
  public function getInitState(): string {
    return $this->state->get($this->systemTokenInitName);
  }

  /**
   * Gets the system access token.
   *
   * @return string|bool
   *   Returns the token string on success and FALSE on failure.
   */
  public function getSystemAccessToken() {
    /** @var \League\OAuth2\Client\Token\AccessTokenInterface $access_token */
    $access_token = $this->state->get($this->systemTokenName);
    if (!$access_token) {
      $this->logger->error('FATAL: System token is still empty. Initialize the system refresh token.');
      return FALSE;
    }

    if ($access_token->getExpires() && $access_token->hasExpired()) {
      $access_token = $this->refreshSystemAccessToken($access_token);

      if (!$access_token) {
        // @todo This should never ever happen. We might need a list of backup tokens for that case.
        $this->logger->emergency('FATAL: Could not refresh system token');
        return FALSE;
      }
    }

    return $access_token->getToken();
  }

  /**
   * Refreshes a given access_token.
   *
   * @param \League\OAuth2\Client\Token\AccessToken $access_token
   *   The access token to refresh.
   *
   * @return \League\OAuth2\Client\Token\AccessToken|bool
   *   The new access token on success, FALSE on failure.
   */
  protected function refreshSystemAccessToken(AccessToken $access_token) {
    while (!$this->lock->acquire($this->lockName)) {
      // Wait for someone else to update the access token.
      $this->lock->wait($this->lockName, 10);

      // Try to read the access token again.
      $this->state->resetCache();
      $new_access_token = $this->state->get($this->lockName);

      if ($new_access_token && !$new_access_token->hasExpired()) {
        return $new_access_token;
      }
    }

    try {
      $refresh_token = $access_token->getRefreshToken();
      $access_token = $this->oauth2Provider->getAccessToken('refresh_token', [
        'refresh_token' => $refresh_token,
      ]);

      if (!$access_token) {
        throw new \Exception('Access token failed to refresh.');
      }

      $this->state->set($this->systemTokenName, $access_token);
    }
    catch (\Exception $e) {
      // As a last resort try to get the access_token from state again.
      $this->state->resetCache();
      $new_access_token = $this->state->get($this->systemTokenName);

      if ($new_access_token && !$new_access_token->hasExpired()) {
        $access_token = $new_access_token;
      }
    }

    $this->lock->release($this->lockName);
    return $access_token;
  }

}
