<?php

namespace Drupal\quip_token_manager\Controller;

use Drupal\oauth2_token_manager\Controller\ValidationControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class QuipValidationController.
 */
class QuipValidationController extends ValidationControllerBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('quip_token_manager.token_manager'));
  }

}
