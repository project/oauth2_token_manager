<?php

namespace Drupal\quip_token_manager\Commands;

use Drupal\oauth2_token_manager\Commands\TokenGeneratorCommandsBase;

/**
 * A Drush commandfile.
 */
class QuipTokenGeneratorCommands extends TokenGeneratorCommandsBase {}
