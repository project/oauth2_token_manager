<?php

namespace Drupal\quip_token_manager;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\oauth2_token_manager\Oauth2TokenManagerServiceProviderBase;

/**
 * Register arguments for services in this module.
 */
class QuipTokenManagerServiceProvider extends Oauth2TokenManagerServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {
    $this->doAlter($container, 'quip_token_manager.oauth2_options', 'QUIP_OAUTH_');
  }

}
