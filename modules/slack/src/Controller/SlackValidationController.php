<?php

namespace Drupal\slack_token_manager\Controller;

use Drupal\oauth2_token_manager\Controller\ValidationControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SlackValidationController.
 */
class SlackValidationController extends ValidationControllerBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('slack_token_manager.token_manager'));
  }

}
