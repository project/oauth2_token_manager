<?php

namespace Drupal\slack_token_manager\Commands;

use Drupal\oauth2_token_manager\Commands\TokenGeneratorCommandsBase;

/**
 * A Drush commandfile.
 */
class SlackTokenGeneratorCommands extends TokenGeneratorCommandsBase {}
