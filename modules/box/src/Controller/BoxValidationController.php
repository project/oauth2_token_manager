<?php

namespace Drupal\box_token_manager\Controller;

use Drupal\oauth2_token_manager\Controller\ValidationControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BoxValidationController.
 */
class BoxValidationController extends ValidationControllerBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('box_token_manager.token_manager'));
  }

}
