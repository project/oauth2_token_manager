<?php

namespace Drupal\box_token_manager\Commands;

use Drupal\oauth2_token_manager\Commands\TokenGeneratorCommandsBase;

/**
 * A Drush commandfile.
 */
class BoxTokenGeneratorCommands extends TokenGeneratorCommandsBase {}
